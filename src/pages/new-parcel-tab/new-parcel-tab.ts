import { Component, ViewChild, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Nav, Events } from 'ionic-angular';
import { HomePage } from '../home/home';
import { RecordingViewPage } from '../recording-view/recording-view';
import { ParcelRecordProvider } from '../../providers/parcel-record/parcel-record';
import { DispatchProvider } from '../../providers/dispatch/dispatch';

/**
 * Generated class for the NewParcelTabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  defaultHistory:['HomePage']
})
@Component({
  selector: 'page-new-parcel-tab',
  templateUrl: 'new-parcel-tab.html',
})
export class NewParcelTabPage  implements OnInit{
  ngOnInit(): void {
    this.getParcelsToDispatch()
  }
  @ViewChild(Nav) nav: Nav;

  DetailView = "AddParcelItemPage"
  ParcelData:parcel[];

  constructor(public navCtrl: NavController, public events:Events,public navParams: NavParams, 
    public parcel:ParcelRecordProvider, public dispatchParcel : DispatchProvider) {
    this.events.subscribe("parcel:recorded",out=>{
     
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewParcelTabPage');
  }
  ionViewWillEnter(){
    this.getParcelsToDispatch()
  }
  
  goto(intent){
    this.navCtrl.push(`${intent}Page`)
  }

  getParcels(){
    this.parcel.getAll().subscribe(parcelData=>this.ParcelData = parcelData.data,
    error=>{
      throw error
    })
  }

  getParcelsToDispatch(){
    this.dispatchParcel.getParcelsToDispatch().subscribe(parcels=>{
      this.ParcelData = parcels.data;
      this.ParcelData.reverse()
    },error=>{throw error})
  }
  
}
