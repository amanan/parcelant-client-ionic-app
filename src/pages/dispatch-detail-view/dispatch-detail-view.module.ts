import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DispatchDetailViewPage } from './dispatch-detail-view';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    DispatchDetailViewPage,
  ],
  imports: [
    IonicPageModule.forChild(DispatchDetailViewPage),
    ComponentsModule
  ],
})
export class DispatchDetailViewPageModule {}
