import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NavController } from 'ionic-angular';

/**
 * Generated class for the ParcelListComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'parcel-list',
  templateUrl: 'parcel-list.html'
})
export class ParcelListComponent implements OnInit {
  ngOnInit(): void {
    console.log(this.type)
    console.log(this.ParcelData)
    console.log('hasCheckbox',this.hasCheckbox)
  }

  text: string;
  @Input('DetailView') DetailView;
  @Input('ParcelData') ParcelData:any[];
  @Input('SelectedParcel') SelectedParcel;
  @Input('type') type:string;
  @Input('routeParam') routeParam;
  @Input('hasCheckbox') hasCheckbox;

  @Output('onParcelSelected') onParcelSelected= new EventEmitter();
  @Output('onParcelUnselected') onParcelUnselected= new EventEmitter(); 

  constructor(public navCtrl:NavController) {
    console.log('Hello ParcelListComponent Component');
    this.text = 'Hello World';
  }

  gotoDetail(item){
    console.log(item)
    this.navCtrl.push(this.DetailView,item)
   // console.log(this.DetailView)
   }

   parcelSelected(e,parcel){
     if(e.checked){
      this.onParcelSelected.emit(parcel)
     }else{
      this.onParcelUnselected.emit(parcel)
     }
   }
   parcelId(index, parcel:parcel){
    return parcel
   }

}
