import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DispatchParcelProvider } from '../../providers/dispatch-parcel/dispatch-parcel';
import { ParcelSelectionProvider } from '../../providers/parcel-selection/parcel-selection';

/**
 * Generated class for the DispatchDetailViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dispatch-detail-view',
  templateUrl: 'dispatch-detail-view.html',
})
export class DispatchDetailViewPage implements OnInit{

  ngOnInit(): void {
    console.log("parcel", this.Parcel)
    this.getSelectedItems()
    this.getParcelItems()
    
  }
  private Parcel:parcel;
  ParcelItems:any[]
  SelectedItems:any[]
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private dispatchParcel:DispatchParcelProvider,
    private parcelSelection:ParcelSelectionProvider) {

    this.Parcel = navParams.data;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DispatchDetailViewPage');
  }

  getParcelItems(){
    this.dispatchParcel.getPracelItemsToDispatch(this.Parcel.id)
    .subscribe(items=>{
      this.ParcelItems = items.data
    },error=>{
      throw error
    })
  }

  parcelItemSelected(item){
    console.log(item)
   this.parcelSelection.addSelectedParcelItem(this.Parcel.id,item.id)
  }

  parcelItemUnselected(item){
    console.log("parcel item unselected",item)
    this.parcelSelection.removeParcelItem(this.Parcel.id,item.id)
  }

  getSelectedItems(){
    this.parcelSelection.getSelectedParcelItems(this.Parcel.id).then(parent=>{
      console.log("items",parent)
      this.SelectedItems = parent
    })
  }
}
