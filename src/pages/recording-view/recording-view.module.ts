import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecordingViewPage } from './recording-view';

@NgModule({
  declarations: [
    RecordingViewPage,
  ],
  imports: [
    IonicPageModule.forChild(RecordingViewPage),
  ]
})
export class RecordingViewPageModule {}
