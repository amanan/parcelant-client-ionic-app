import { Injectable } from '@angular/core';
import { HttpServiceProvider } from '../http/http';
import { UserInfoProvider } from '../user-info/user-info';

/*
  Generated class for the TransitUserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TransitUserProvider {

  constructor(public httpProvider: HttpServiceProvider,
              public userInfoProvider:UserInfoProvider
  )
  {
    console.log('Hello TransitUserProvider Provider');
  }

  getTransitUsers(){
    return this.httpProvider.Get(`api/TransitUser/${this.userInfoProvider.UserInfo.clientId}`)
  }



}
