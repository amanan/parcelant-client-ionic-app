import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ParcelRecordProvider } from '../../providers/parcel-record/parcel-record';

/**
 * Generated class for the AddParcelItemPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-parcel-item',
  templateUrl: 'add-parcel-item.html',
})
export class AddParcelItemPage {
  parcel:parcel;
  items:any[];
  totalAmount: number = 0;
  totalItems: number;
  description;
  constructor(public navCtrl: NavController,public alertCtrl:AlertController, public navParams: NavParams, public parcelRecord:ParcelRecordProvider) {
    this.parcel =navParams.data;
    this.getParcelItems()

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddParcelItemPage');
  }
  presentPrompt() {
    let alert = this.alertCtrl.create({
      title: 'Item',
      enableBackdropDismiss:false,
      inputs: [
       {
          name: 'Description',
          type:'text',
          placeholder:"Description",
          value:this.description
        },
        {
          name: 'Quantity',
          type: 'number',
          placeholder:"Quantity"
        },
        {
          name: 'Amount',
          type: 'number',
          placeholder:'Amount'
        }
      ],
      buttons: [
       
        {
          text: 'Save',
          role:'Save',
          cssClass:'btnSave',
          handler: data => {
            if(data.Description && data.Quantity && data.Amount){
              this.AddItem(data.Description,data.Quantity,data.Amount)
            } 
            
          }
        },
        {
          text: 'Next',
          cssClass:'btnNext',
          handler: data => {
            if(data.Description && data.Quantity && data.Amount){
              this.AddItem(data.Description,data.Quantity,data.Amount)
              this.description = ""
              
            }
            return false
          }
        }
      ]
    });
    alert.present();
  }

  AddItem(description,quantity,amount){
    let item ={
      description:description,
      quantity:quantity,
      amount:amount
    }

    this.parcelRecord.AddParcelItem(this.parcel.id,item).subscribe(item=>{
      console.log(item)
      this.items.unshift(item.data)
    },error=>{
      throw error
    })
  }

  getParcelItems(){
    this.parcelRecord.getparcelItems(this.parcel.id).subscribe(items=>{
      this.items = items.data;
      this.totalItems = this.items.length;
      this.computeTotalAmount()
    },error=>{
      throw error
    })
  }

  computeTotalAmount(){

    if (this.items) {
      this.items.forEach(element => {
       this.totalAmount +=element.amount;  
      });

      
   
  }
}

  



}
