import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { ReceiveDetailViewPage } from '../receive-detail-view/receive-detail-view';
import { ReceiveParcelProvider } from '../../providers/receive-parcel/receive-parcel';
import { ParcelSelectionProvider } from '../../providers/parcel-selection/parcel-selection';

/**
 * Generated class for the ReceiveViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-receive-view',
  templateUrl: 'receive-view.html',
})
export class ReceiveViewPage implements OnInit {
  //dispatchParcel: any;
  SelectedParcel: any[];
  
  public DetailView = ReceiveDetailViewPage
  ParcelData:parcel[];
  public totalParcelsSelected:number =0;
  public totalItemsSelected:number=0;
  private ParcelItemsSelectedToReceive:string = "ParcelItemsSelectedToReceive";
  private ParcelSelectedToReceive:string = "ParcelSelectedToReceive";
  private ParcelUnSelectedToReceive:string = "ParcelUnSelectedToReceive";
  

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public receiveParcel:ReceiveParcelProvider,
              private parcelSelection:ParcelSelectionProvider,
              public events:Events) {

                // fire when parcel is selected
                this.events.subscribe(this.ParcelSelectedToReceive, out=>{
                  this.getSelectedParcels()
                })
                
                // fire when parcel is unselected
                this.events.subscribe(this.ParcelUnSelectedToReceive , out=>{
                  this.getSelectedParcels()
                })

  }

  ngOnInit(): void {
    this.parcelSelection.StorageKey = this.ParcelSelectedToReceive;
    this.parcelSelection.ParcelSelectedNotificationName = this.ParcelSelectedToReceive;
    this.parcelSelection.ParcelUnSelectedNotificationName = this.ParcelUnSelectedToReceive;

    this.getSelectedParcels()
    this.getParcels()
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReceiveViewPage');
  }
  
  getParcels(){
    this.receiveParcel.getParcelsToReceive().subscribe(parcels=>{
      if(parcels.data){
      this.ParcelData = parcels.data;
      this.ParcelData.reverse()
      }
    })
  }
  
  parcelUnselected(data){
    console.log("parcel Unselected", data)
    this.parcelSelection.removeParcel(data.id);
  }
  parcelSelected(data){
    this.receiveParcel.getParcelItemsToReceive(data.id).subscribe(items=>{
     let itemids = []
     let length = items.data.length
      items.data.forEach(element => {
        itemids.push(element.id)
        if(itemids.length==length){
          this.parcelSelection.addSelectedParcel(data.id,itemids)
        }
      });  
    
    })
   
  }
  getSelectedParcels(){
    this.parcelSelection.getParcelsSelected()
    .then(data=>{
      this.SelectedParcel = data
      if(data){
        this.SelectedParcel = data
      this.totalParcelsSelected = Object.keys(data).length
      this.totalItemsSelected =0;
      for (const key in data) {
        if (data.hasOwnProperty(key)) {
          this.totalItemsSelected += data[key].length;
          
        }
      }
    }else{
      this.SelectedParcel = []
    }
    })
  }
  
}
