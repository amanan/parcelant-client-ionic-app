import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpServiceProvider } from '../http/http';
import { UserInfoProvider } from '../user-info/user-info';

/*
  Generated class for the DispatchProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DispatchProvider {

  clientId;
  branchId;
  appUserId;
  constructor(public httpProvider: HttpServiceProvider,public userInfo:UserInfoProvider) {
    console.log('Hello DispatchProvider Provider');
    this.clientId = userInfo.UserInfo.clientId;
    this.branchId = userInfo.UserInfo.branchId;
    this.appUserId = userInfo.UserInfo.id;

  }

  getParcelsToDispatch(){
   return this.httpProvider.Get(`/api/Dispatch/Parcelstodispatch/${this.clientId}/${this.branchId}`)
  }
  
}
