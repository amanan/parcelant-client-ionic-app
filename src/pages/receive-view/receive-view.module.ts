import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReceiveViewPage } from './receive-view';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    ReceiveViewPage,
  ],
  imports: [
    IonicPageModule.forChild(ReceiveViewPage),
    ComponentsModule,
  ],
})
export class ReceiveViewPageModule {}
