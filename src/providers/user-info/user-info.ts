import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpServiceProvider } from '../http/http';
import { StorageServiceProvider } from '../storage/storage';
import { Events } from 'ionic-angular';

/*
  Generated class for the UserInfoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserInfoProvider {

  public UserInfo: Userinfo;

  constructor(public httpService: HttpServiceProvider, public storageService:StorageServiceProvider, public events:Events) {
    console.log('Hello UserInfoProvider Provider');
    this.events.subscribe('userinfo:saved',out=>{
      this.getUserInfo()
    })
   this.getUserInfo()
  }


  fetchUserInfo(){
    return this.httpService.Get('api/appuserinfo')
  }

  getUserInfo(){
    this.storageService.get('userinfo').then(user=>{
      this.UserInfo=user
      this.events.publish("userinfo:initialized");
    
    }).catch(error=>{
      throw error
    })
  }


}

interface Userinfo{
  id: number,
  username: string,
  fullname: string,
  phone: string,
  active: boolean,
  branchName: string,
  branchId: number,
  clientName: string,
  clientId: number,
  createdAt: string,
  updatedAt: string
}

interface UserinfoResponse{

  error: boolean,
  errorMessage: object,
  status: number,
  data:Userinfo
}
