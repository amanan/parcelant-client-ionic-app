import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpServiceProvider } from '../http/http';
import { UserInfoProvider } from '../user-info/user-info';

/*
  Generated class for the ReceiveParcelProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ReceiveParcelProvider {

  
  clientId;
  branchId;
  appUserId;
  constructor(public httpProvider: HttpServiceProvider,public userInfo:UserInfoProvider) {
    console.log('Hello ReceiveParcelProvider Provider');
    this.clientId = userInfo.UserInfo.clientId;
    this.branchId = userInfo.UserInfo.branchId;
    this.appUserId = userInfo.UserInfo.id;

  }

  getParcelsToReceive(){
    return this.httpProvider.Get(`api/Receive/Parcels-To-Receive/${this.clientId}/${this.branchId}`)
  }

  getParcelItemsToReceive(parcelId){
    return this.httpProvider.Get(`api/Receive/Parcelitems-to-Receive/${this.clientId}/${this.branchId}/${parcelId}`)
  }

  getParcelsSelected(): any {
    
  }

}
