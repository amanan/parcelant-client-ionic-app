import { Component} from '@angular/core';
import { NavController, Nav} from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { LoginViewPage } from '../login-view/login-view';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
   
  constructor(public navCtrl: NavController, public authProvider:AuthProvider) {
 
  }

  goto(intent){
    this.navCtrl.push(`${intent}Page`)
  }

 
}
