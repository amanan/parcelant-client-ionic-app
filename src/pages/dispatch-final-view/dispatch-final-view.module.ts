import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DispatchFinalViewPage } from './dispatch-final-view';

@NgModule({
  declarations: [
    DispatchFinalViewPage,
  ],
  imports: [
    IonicPageModule.forChild(DispatchFinalViewPage),
  ],
})
export class DispatchFinalViewPageModule {}
