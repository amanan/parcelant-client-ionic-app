import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddParcelPage } from './add-parcel';

@NgModule({
  declarations: [
    AddParcelPage,
  ],
  imports: [
    IonicPageModule.forChild(AddParcelPage),
  ],
})
export class AddParcelPageModule {}
