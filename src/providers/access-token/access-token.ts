import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { StorageServiceProvider } from '../storage/storage';
import { Events } from 'ionic-angular';

/*
  Generated class for the AccessTokenProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AccessTokenProvider {

  public token : string
  constructor(public storageService:StorageServiceProvider, public events:Events) {
    console.log('Hello AccessTokenProvider Provider');
    this.GetToken();

    this.events.subscribe('token:saved',out=>{
      this.GetToken();
    })


  }

  private GetToken(){
     this.storageService.get('access_token').then(token=>{
       this.token = token
       this.events.publish('token:initalized')
      })
  }

}
